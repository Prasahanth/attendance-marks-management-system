import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GenerateidService {

  branchFromComponent;
  loggedinStatus:boolean;
  loggedinUser:any;
  studentObject:any;
  bcode;
  constructor(private hc:HttpClient,private router:Router) { }
  
  logout(){
    var s=confirm("Are you Sure u want to logout?")
    if(s==true){
      this.loggedinStatus=false;
      this.router.navigate(['/home'])
    }
    
  }


toCommonComponent(){
 // console.log(this.branchFromComponent);
  return this.branchFromComponent;
}




  
  generate(obj):Observable<any>{
    this.bcode=obj.branchcode;
    return this.hc.post('/generateid',obj)

  }
  register(obj1):Observable<any>{
    obj1.branchcode=this.bcode;
    return this.hc.post('/save',obj1)
  }
  readid():Observable<any>{
    return this.hc.get('/getid')
  }
  //read student by branch
  readstu(obj):Observable<any>{
    return this.hc.get(`/readstu/${obj}`)
  }
  //delete
  delete(de):Observable<any>{
    return this.hc.delete(`/delete/${de.ph}`)
  }
  //update
  update(up):Observable<any>{
    return this.hc.put('/update',up)
  }
//readBy year
  readByYear(byyear):Observable<any>{
    console.log(byyear); 
    return this.hc.post('/readbyyear',byyear)
  }
  
  loginstudent(obj):Observable<any>{
    console.log(obj);
    return this.hc.post('/login',obj)
  }
  setAttendence(obj):Observable<any>{
    return this.hc.post('/uploadattendance',obj)
  }

  //get all students attendance (admin)
  getAttendene():Observable<any>{
    return this.hc.get('/getattendence')
  }
//get one student attendance (student)
  getStudentAttendance(data):Observable<any>{
    console.log(data);
    
    return this.hc.get(`/readStudentAttendance/${data}`)
  }
  //set all student marks
  setMarks(obj):Observable<any>{
    return this.hc.post('/uploadmarks',obj)
  }
  //get all student marks(admin)
  getmarks():Observable<any>{
    return this.hc.get('/getmarks')
  }
  //get student marks (student)
  getStudentMarks(data):Observable<any>{
    console.log(data);
    
    return this.hc.get(`/readStudentMarks/${data}`)
  }
}
