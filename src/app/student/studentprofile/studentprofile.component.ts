import { Component, OnInit } from '@angular/core';
import { GenerateidComponent } from 'src/app/admin/generateid/generateid.component';
import { GenerateidService } from 'src/app/generateid.service';

@Component({
  selector: 'app-studentprofile',
  templateUrl: './studentprofile.component.html',
  styleUrls: ['./studentprofile.component.css']
})
export class StudentprofileComponent implements OnInit {
  constructor(private gi:GenerateidService) { }
  studentObject=this.gi.loggedinUser;
  
  ngOnInit() {
    console.log(this.studentObject);
    
  }


}
