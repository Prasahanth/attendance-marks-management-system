import { Component, OnInit } from '@angular/core';
import { GenerateidService } from 'src/app/generateid.service';

@Component({
  selector: 'app-readmarks',
  templateUrl: './readmarks.component.html',
  styleUrls: ['./readmarks.component.css']
})
export class ReadmarksComponent implements OnInit {

  constructor(private gi:GenerateidService) { }
  studentdata=this.gi.loggedinUser;
  studentid=this.gi.loggedinUser.studentid;
  data;
  ngOnInit() {
    console.log(this.studentid);
    this.gi.getStudentMarks(this.studentid).subscribe((res)=>{
      if(res['message']=="no data found")
      {
        alert("no data found");
      }
      else{
        this.data=res['message'];
        console.log(this.data);
        
      }
    })


  }

}
