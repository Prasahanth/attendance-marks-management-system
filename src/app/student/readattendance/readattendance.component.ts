import { Component, OnInit } from '@angular/core';
import { GenerateidService } from 'src/app/generateid.service';

@Component({
  selector: 'app-readattendance',
  templateUrl: './readattendance.component.html',
  styleUrls: ['./readattendance.component.css']
})
export class ReadattendanceComponent implements OnInit {
data;
  constructor(private gi:GenerateidService) { }
studentdata=this.gi.loggedinUser;
studentid=this.gi.loggedinUser.studentid;


  ngOnInit() {
    console.log(this.studentid);
    this.gi.getStudentAttendance(this.studentid).subscribe((res)=>{
      if(res['message']=="no data found")
      {
        alert("no data found");
      }
      else{
        this.data=res['message'];
        console.log(this.data);
        
      }
    })
  }

}
