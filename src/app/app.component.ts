import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
import { GenerateidService } from './generateid.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  constructor(private spinner: NgxSpinnerService,private router:Router,public gi:GenerateidService) {
   // this.router.navigate(['/home'])
  }

status=this.gi.loggedinStatus;

  ngOnInit() {
    /** spinner starts on init */
    this.spinner.show();
 
    setTimeout(() => {
      /** spinner ends after 2 seconds */
      this.spinner.hide();
    }, 5000);
  }
 logout()
 {
   this.gi.logout();
 }
}
