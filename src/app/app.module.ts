import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule} from '@angular/forms';
import{HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { DepartmentsComponent } from './departments/departments.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { LoginComponent } from './login/login.component';
import { CseComponent } from './cse/cse.component';
import { EceComponent } from './ece/ece.component';
import { CivilComponent } from './civil/civil.component';
import { MechComponent } from './mech/mech.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { AdminModule } from './admin/admin.module';
import { StudentModule } from './student/student.module';
import { CommonPipe } from './common.pipe';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DepartmentsComponent,
    AboutusComponent,
    ContactusComponent,
    LoginComponent,
    CseComponent,
    EceComponent,
    CivilComponent,
    MechComponent,
    CommonPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    AdminModule,
    StudentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
