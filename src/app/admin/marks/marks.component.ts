import { Component, OnInit } from '@angular/core';
import { GenerateidService } from 'src/app/generateid.service';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit {

  constructor(private gi:GenerateidService) { }
data;
  ngOnInit() {
    this.gi.getmarks().subscribe((res)=>{
      if(res['message']==="no data found"){
            alert("no data found")
      }
      else{
            this.data=res["message"];
              console.log(this.data);
    
      }
      
    
    
    })
  }


  file:File;
  fileUpload(filedata){
    this.file=filedata.target.files[0];
  }
  uploadmarks(data){

    let formdata = new FormData();
    formdata.append("marks",this.file,this.file.name);
    this.gi.setMarks(formdata).subscribe((res)=>{
      if(res["message"]=="Marks Sheet uploaded successfully")
      {
               alert(res["message"]);
              this.ngOnInit();
      }
      else if(res["err_desc"]=="Corupted excel file")
      {
        alert(res["err_desc"]);
        this.ngOnInit();

      }
})
  }
}
