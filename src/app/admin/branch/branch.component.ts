import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GenerateidService } from 'src/app/generateid.service';

@Component({
  selector: 'app-branch',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {

  constructor(private router:Router,private gi:GenerateidService) { }

  ngOnInit() {
   
  }

  branchName;

  submitBranch(obj){
   // console.log(obj);
    this.branchName=obj;
    this.gi.branchFromComponent=this.branchName;
   // console.log("bname is",this.gi.branchFromComponent);
   // console.log(this.branchName)
    this.router.navigate(['/adminprofile/common'])
  }


}
