import { Component, OnInit } from '@angular/core';
import { GenerateidService } from 'src/app/generateid.service';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {

  constructor(private gi:GenerateidService) { }
 data:[]=[];
  ngOnInit() {
this.gi.getAttendene().subscribe((res)=>{
  if(res['message']==="no data found"){
        alert("no data found")
  }
  else{
        this.data=res["message"];
          console.log(this.data);

  }
  


})

  }
  file:File;
  fileUpload(filedata){
    this.file=filedata.target.files[0];
  }
  uploadattendance(data)
  {
    console.log(data);
    
    let formdata = new FormData();
    formdata.append("branchname",data.branchname);
    formdata.append("year",data.year);
    formdata.append("attendance",this.file,this.file.name);
    this.gi.setAttendence(formdata).subscribe((res)=>{
      if(res["message"]=="Attendence Sheet uploaded successfully")
      {
               alert(res["message"]);
               this.ngOnInit();
      }
      else if(res["err_desc"]=="Corupted excel file")
      {
        alert(res["err_desc"]);
        this.ngOnInit();

      }
})

  }
}