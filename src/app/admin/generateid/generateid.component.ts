import { Component, OnInit } from '@angular/core';
import { GenerateidService } from 'src/app/generateid.service';

@Component({
  selector: 'app-generateid',
  templateUrl: './generateid.component.html',
  styleUrls: ['./generateid.component.css']
})
export class GenerateidComponent implements OnInit {

  constructor(private gi:GenerateidService) { }

  ngOnInit() {
    this.gi.readid().subscribe((dataArray)=>{
      this.data=dataArray['message'];
    })
  }

  
data:any=[];
  generateId(data1)
  {
    console.log(data1);
    data1.count=0;
    
this.gi.generate(data1).subscribe((res)=>{
  if(res['message']=='successful'){
    alert(res['message']);
    this.ngOnInit();
  }
  else if(res['message']=='id generated already')
  {
    alert("barnch id already generated");
    this.ngOnInit();
  }

})
    // this.data.push(obj);

    // console.log(obj);

  }
}
