import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

 transform(stdObj:any, searchTerm: any): any[] {
   console.log(stdObj,searchTerm);
    if(!searchTerm)
    {
    return stdObj;
    }
    else
    {
    return stdObj.filter(std=> std["fn"].toLowerCase().indexOf(searchTerm.toLowerCase())!==-1
          )
        }

}
}
