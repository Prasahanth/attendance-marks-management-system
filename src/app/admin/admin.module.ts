import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminprofileComponent } from './adminprofile/adminprofile.component';
import { GenerateidComponent } from './generateid/generateid.component';
import { RegisterstudentComponent } from './registerstudent/registerstudent.component';
import { BranchComponent } from './branch/branch.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { MarksComponent } from './marks/marks.component';
import { FormsModule } from '@angular/forms';
import { CommonComponent } from './common/common.component';
import { HttpClientModule } from '@angular/common/http';
import { SearchPipe } from './search.pipe';


@NgModule({
  declarations: [AdminprofileComponent, GenerateidComponent, RegisterstudentComponent, BranchComponent, AttendanceComponent, MarksComponent, CommonComponent, SearchPipe],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    HttpClientModule
  ]
})
export class AdminModule { }
