import { Component, OnInit } from '@angular/core';
import { GenerateidService } from 'src/app/generateid.service';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION='.xlsx';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'app-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.css']
})


export class CommonComponent implements OnInit {
 
  constructor(private gi:GenerateidService) { }
department;

//download pdf file 
downloadPDF(){
 
  const doc = new jsPDF()
  var col=["FULL NAME","STUDENT ID","GENDER","PHONE NO","COURSE","ADDRESS","E-MAIL","YEAR","SSC","INTER","DATE  "]
  var rows=[];
  this.fromService.forEach(element=>{let name=element.fn+element.ln;
                                    let id=element.studentid;  
                                    let gender=element.m;
                                    let pno=element.ph;
                                    let course=element.course;
                                    let  address=element.ad;
                                    let email=element.email;
                                    let year=element.year;
                                    let ssc=element.ssc;
                                    let inter=element.inter;
                                    let date=new Date();
                                    let temp=[name,id,gender,pno,course,address,email,year,ssc,inter,date]
                                    rows.push(temp)
                         })
        doc.autoTable(col,rows,{
          theme:'grid'
           })
          doc.save('first.pdf')
              }
              
              
             
//download Exel file
  public downloadFile(): void {
    let data1=[];
    


    this.fromService .forEach(element => {
      let data2={}
         data2["year"]=element.year,data2["course"]=element.course,data2["date"]=element.date,
         data2["studentid"]=element.studentid,data2["attendance"]=element.attendance
         data1.push(data2)
               
    });
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(data1);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames:
    ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type:
    'array' });
    this.saveAsExcelFile(excelBuffer, 'excelFileName');
    }
    private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type:EXCEL_TYPE});
    FileSaver.saveAs(data, fileName+'_export_'+new Date().getTime()+EXCEL_EXTENSION);
    }


stdObj;
searchTerm;

  ngOnInit() {
    //from service
    this.department=this.gi.toCommonComponent();
    this.gi.readstu(this.department).subscribe((dataArray)=>{
      this.fromService=dataArray['message'];
 
    })
  }
  
  obj={'year':0,'course':''};
  onYear(year:any){
    if(year==='all'){
      console.log(year);
      
      this.ngOnInit();
    }
    else{
      console.log(year);
    this.obj.year=year;
    this.obj.course=this.department;
    console.log(this.obj);
this.gi.readByYear(this.obj).subscribe((dataArray)=>{
  if(dataArray['message']==="nodatafound")
  {
      alert("nodatafound")
  }
  else
  {
    this.fromService=dataArray['message'];
  }
  
})
    }

    
    

  }


da:any=[];
  update(ob){
    console.log(ob);
    this.da=ob;
  }
  fromService:any=[];
  
  
  registerStudent(obj)
 {
  console.log(obj);
  this.gi.register(obj).subscribe((res)=>{
    if(res['message']=='updated'){
      alert("successfully registerd");
    }
    else if(res['message']=="generateid first"){
      alert("generateid first");
    }
    
   
    this.ngOnInit();
  })
}
deleteStudent(de){
  console.log(de);
  var a=confirm("Are u sure u want to delete data");
  if(a==true)
  {
    this.gi.delete(de).subscribe((res)=>{
    alert(res['message'])
    this.ngOnInit();
  }
  )}
  else
  {
    this.ngOnInit();
  }
  
}
updateStudent(up){
  console.log(up);
  this.gi.update(up).subscribe((res)=>{
    alert(res['message'])
  })
}

}
