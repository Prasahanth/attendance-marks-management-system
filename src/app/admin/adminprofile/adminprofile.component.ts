import { Component, OnInit } from '@angular/core';
import { GenerateidService } from 'src/app/generateid.service';

@Component({
  selector: 'app-adminprofile',
  templateUrl: './adminprofile.component.html',
  styleUrls: ['./adminprofile.component.css']
})
export class AdminprofileComponent implements OnInit {

  constructor(private gi:GenerateidService) { }
username=this.gi.loggedinUser;
  ngOnInit() {
  }

}
