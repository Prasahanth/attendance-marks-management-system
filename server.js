//load express function
const exp=require("express");
//var smtpTransport = require('nodemailer-smtp-transport');

//create express object
const app=exp();
//import mongo client
const mc=require("mongodb").MongoClient;
var dbo;

const dbUrl="mongodb://prashanth:prashanth@cluster0-shard-00-00-qgn0c.mongodb.net:27017,cluster0-shard-00-01-qgn0c.mongodb.net:27017,cluster0-shard-00-02-qgn0c.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";

//connect to database using dbUrl
mc.connect(dbUrl,{useNewUrlParser:true,useUnifiedTopology:true},(error,client)=>{
    if(error)
    {
        console.log("error in db connection");
    }
    else
    {
        //get database object from client
        dbo=client.db("project");
        console.log("connected to db");
    }
})

//

//imort body parser
const bp=require("body-parser");


//import multer module
const multer=require('multer');
//import xlsx-to-json-lc module
const xlsxtojson=require("xlsx-to-json-lc");
//import xls-to-json-lc module
const xlstojson=require("xls-to-json-lc");

//import nodemailer module
const nodemailer=require("nodemailer");

//path
const path=require("path");
app.use(exp.static(path.join(__dirname,'./dist/project/')))

//multers disk storage settings
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
    cb(null, './uploadattendance/')
    },
    filename: function (req, file, cb) {
    var datetimestamp = Date.now();
    cb(null, `${new Date().getTime()}_${file.originalname}`)
    }
   });
   // upload middleware
const upload = multer({ storage: storage});
// convert excel to json route
  app.post("/uploadattendance",upload.single('attendance'),(req,res)=>{
    if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1]
   === 'xlsx'){
    exceltojson=xlsxtojson;
    }
     else
      {
    exceltojson=xlstojson;
    }
    try {
    exceltojson({
    input: req.file.path, //the same path where we uploaded our file
    output: null, //since we don't need output.json
    lowerCaseHeaders:true
    }, function(err,result){
    if(err) {
    return res.json({error_code:1,err_desc:err, data: null});
    }else{
    console.log("result is",result);
    dbo.collection("attendance").insertMany(result,(err,data)=> {
        if(err) throw err;
    console.log(data);
    res.json({error_code:0,err_desc:null, data:
   data["ops"],"message":"Attendence Sheet uploaded successfully"});
    });
    }
    });

    } catch (e){
    res.json({error_code:1,err_desc:"Corupted excel file"});
    }
    });
//get request handler to get attendence data
    app.get('/getattendence',(request,response)=>{
        //read data from collection and convert to array
    dbo.collection("attendance").find().toArray((error,dataArray)=>{
        if(error)
        {
    console.log("error in reading",error);
        }
        else if(dataArray.length==0)
        {
            response.send({message:"no data found"})
        }
        else
        {
            response.send({message:dataArray})
        }
    
    })
    })

    //multers disk storage settings
var storage1 = multer.diskStorage({
    destination: function (req, file, cb) {
    cb(null, './uploadattendance/')
    },
    filename: function (req, file, cb) {
    var datetimestamp = Date.now();
    cb(null, `${new Date().getTime()}_${file.originalname}`)
    }
   });
   // upload middleware
const upload1 = multer({ storage: storage1});
// convert excel to json route
  app.post("/uploadmarks",upload1.single('marks'),(req,res)=>{
    if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1]
   === 'xlsx'){
    exceltojson=xlsxtojson;
    }
     else
      {
    exceltojson=xlstojson;
    }
    try {
    exceltojson({
    input: req.file.path, //the same path where we uploaded our file
    output: null, //since we don't need output.json
    lowerCaseHeaders:true
    }, function(err,result){
    if(err) {
    return res.json({error_code:1,err_desc:err, data: null});
    }
    dbo.collection("marks").insertMany(result,(err,data) => {
    console.log(data);
    res.json({error_code:0,err_desc:null, data:
   data["ops"],"message":"Marks Sheet uploaded successfully"});
    });
   
    });
    } catch (e){
    res.json({error_code:1,err_desc:"Corupted excel file"});
    }
    });
//get request handler to get marks data
app.get('/getmarks',(request,response)=>{
    //read data from collection and convert to array
dbo.collection("marks").find().toArray((error,dataArray)=>{
    if(error)
    {se
console.log("error in reading",error);
    }
    else if(dataArray.length==0)
    {
        response.send({message:"no data found"})
    }
    else
    {
        response.send({message:dataArray})
    }

})
})    
var year;
var branch;
var yearcode;
var ye;
var id;
//generateid request handler (post)
app.use(exp.json());
app.post('/generateid',(request,response)=>{
    console.log(request.body);
    dbo.collection("generateid").find({year:request.body.year,branchname:request.body.branchname}).toArray((error,data)=>{
        if(error){
            console.log("error in finding",error)
        }
        else if(data.length===0)
        {
            dbo.collection("generateid").insertOne(request.body,(error,success)=>{
                if(error){
                    console.log("error in insert",error)
                }
                else{
                    response.send({message:'successful'})
                }
            })

        }
        else{
            response.send({message:'id generated already'})
        }
    })
   
})


//register student handler (post)

app.use(exp.json());
app.post('/save',(request,response)=>{
    console.log(request.body);
   
 dbo.collection("generateid").find({year:request.body.year,branchname:request.body.course}).toArray((error,res)=>{
    // console.log(result);
    let result=res[0];
    console.log(result);
    let length=res.length;

    console.log("length is ",length)
    if(error){
         console.log("error in finding",error)
     }
     else if(length==0)
     {
        response.send({message:"generateid first"})
     }
     else{
        year=JSON.stringify(result.year);
        yearcode=year.split("");
        //console.log(year);
        console.log(result.branchname)
        branch=result.branchname;
        //console.log(branchname);
        ye=yearcode[2]+yearcode[3];
        
        id=(ye+branch+result.branchcode)
        console.log("id is",id);
        let ct=++result.count;
         if(result.count<=9){
            request.body.studentid=id+"00"+ct;
            request.body.password=id+"00"+ct
            console.log(request.body.studentid);
            console.log(request.body.password);
         }
         else if(result.count<=99)
         {
            request.body.studentid=id+"0"+ct ;
            request.body.password=id+"0"+ct
            console.log(request.body.studentid);
            console.log(request.body.password);
         }
         else{
            request.body.studentid=id+ct
            request.body.password=id+ct
            console.log(request.body.studentid);
            console.log(request.body.password);
            
            
         }
         dbo.collection("registerstudent").insertOne(request.body,(error,success)=>{
             console.log(request.body);
            if(error){
                console.log("error in insert",error)
            }
            else{
                dbo.collection("generateid").updateOne({year:request.body.year,branchname:request.body.course},
                    {$set:{count:ct}},(error,suc)=>{
                        if(error){
                            console.log("error in update",error);
                            
                        }
                        else{   
                            const transport = nodemailer.createTransport({
                                
                                service: 'gmail',
                                auth: {
                                    user: 'shanthreddy10@gmail.com',
                                    pass: 'shanth65',
                                   
                                },
                                tls: {
                                    rejectUnauthorized: false
                                }
                               
                            
                            });
                            const mailOptions={
                                from: 'shanthreddy10@gmail.com',
                                to: `email:${request.body.email}`,
                                subject: 'student id and password',
                                text: `
                                Hello,${request.body.fn},
                                welcome to FIU university..
                                we have successfully registerd your
                                 profile in ${request.body.course} branch
                                your  username and password:
                                ${request.body.studentid}
                                please login with your credentials 
                                thank you`
                            };
                            transport.sendMail(mailOptions,(error,info) => {
                                if (error) {
                                    console.log("error is",error);
                                }else{
                                    console.log("Message sent",info);
                                    response.send({message:"updated"})
                                }
                               
                            });

//                        response.send({message:"updated"})   
                        }
                    })
               
            }
        }) 
     }
 })


   
})

//generateid (get) request handler
app.get('/getid',(request,response)=>{
    //read data from collection and convert to array
dbo.collection("generateid").find().toArray((error,dataArray)=>{
    if(error)
    {
console.log("error in reading",error);
    }
    else if(dataArray.length==0)
    {
        response.send({message:"no data found"})
    }
    else
    {
        response.send({message:dataArray})
    }

})
})

//branch wise reading get request handler
app.get('/readstu/:course',(request,response)=>{
    //read data from collection and convert to array
dbo.collection("registerstudent").find({course:request.params.course}).toArray((error,dataArray)=>{
    if(error)
    {
console.log("error in reading",error);
    }
    else if(dataArray.length==0)
    {
        response.send({message:"no data found"})
    }
    else
    {
        response.send({message:dataArray})
    }

})
})




//readby year and department (get)
app.post('/readbyyear',(request,response)=>{
    //read data from collection and convert to array

    console.log(request.body)
    let ye=(+request.body.year)
    let de=(request.body.course)

    console.log(ye,de);
    
dbo.collection("registerstudent").find({year:ye,course:de}).toArray((error,dataArray)=>{
    if(error)
    {
console.log("error in reading",error);
    }
    else if(dataArray.length==0)
    {
        console.log('no data found');
        
        response.send({message:"nodatafound"})
    }
    else
    {
        console.log(dataArray)
        response.send({message:dataArray})
    }

})
})

//delete request handler
app.delete('/delete/:ph',(request,response)=>{
    console.log("phone number is",request.params.ph)
    let num=(+request.params.ph)
    dbo.collection("registerstudent").deleteOne({ph:num},(error,success)=>{
        if(error){
            console.log('error in delete',error)
        }
        else{
            response.send({message:'successfully deleted'})
        }
    })
})
//update request handler (put)
app.put('/update',(request,response)=>{
    console.log("data is",request.body);
    dbo.collection("registerstudent").updateOne({ph:request.body.ph},
                    {$set:{fn:request.body.fn,
                            ln:request.body.ln,
                        m:request.body.m,
                    email:request.body.email,
                    course:request.body.course,
                ad:request.body.ad,
            year:request.body.year,
        ssc:request.body.ssc,
    inter:request.body.inter}},(error,success)=>{
        if(error){
            console.log("error in updation",error)
        }
        else{
            response.send({message:'success'})
        }
    })


})

//login reqest handler
app.post('/login',(request,response)=>{
    //read object
    console.log(request.body);
    dbo.collection("registerstudent").findOne({studentid:request.body.id},(error,stuobj)=>{
        if(error){
            console.log("error in finding ",error)
        }
        else if(stuobj==null){
                response.send({message:"invalid-studentid"})
        }
        else{
            if(request.body.pw!==stuobj.password){
                response.send({message:"invalid-password"})
            }
            else{
                console.log(stuobj);
                response.send({message:"successful-login",name:stuobj})
            }
        }
    })
})


//get request handler to get attendence one studentdata
app.get('/readStudentAttendance/:studentid',(request,response)=>{
    //read data from collection and convert to array
    console.log(request.params.studentid);
    
dbo.collection("attendance").find({studentid:request.params.studentid}).toArray((error,dataArray)=>{
    if(error)
    {
console.log("error in reading",error);
    }
    else if(dataArray.length==0)
    {
        response.send({message:"no data found"})
    }
    else
    {
        response.send({message:dataArray})
    }

})
})


//get request handler to get marks of individual studentdata
app.get('/readStudentMarks/:studentid',(request,response)=>{
    //read data from collection and convert to array
    console.log(request.params.studentid);
    
dbo.collection("marks").find({studentid:request.params.studentid}).toArray((error,dataArray)=>{
    if(error)
    {
console.log("error in reading",error);
    }
    else if(dataArray.length==0)
    {
        response.send({message:"no data found"})
    }
    else
    {
        response.send({message:dataArray})
    }

})
})







//assign port number
const port=4000;
app.listen(process.env.PORT || port,()=>{console.log(`server listening on ${port}`)})